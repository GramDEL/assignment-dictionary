section .text

%define addr 0
%macro colon 2
    %%next: dq addr
    db %1, 0
    %2:
%define addr %%next
%endmacro

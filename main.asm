%include "colon.inc"
%include "words.inc"
%define buffer_size 256

extern print_string
extern print_error
extern read_word
extern find_word
extern string_length
extern print_newline
extern exit

global _start

section .rodata
enter_key_msg: db "Enter a key: ", 0
match_msg: db "Match found! The value is: ", 0
no_match_msg: db "Match wasn't found.", 10, 0
long_key_msg: db "The key you entered is too long! (>buffer_size symbols)", 10, 0
empty_key_msg: db 10, "The key you entered is empty!", 10, 0

section .text
_start:
    mov rdi, enter_key_msg
    call print_string

    sub rsp, buffer_size ; выделяем место под буфер
    mov rdi, rsp
    mov rsi, buffer_size
    call read_word
    test rax, rax
    jz .long_key ; если не хватает буфера
    test rdx, rdx
    jz .empty_key ; если ключ пустой

    mov rdi, rax
    mov rsi, addr
    call find_word
    test rax, rax
    jz .no_match ; если нет совпадений

    add rsp, buffer_size ; избавляемся от буфера
    add rax, 8 ; смещаемся на кол-во байтов, равное размеру dq, чтобы получить адрес ключа

    push rax ; сохарняем адрес перед вызовом
    mov rdi, match_msg
    call print_string
    pop rdi ; помещаем адрес ключа в rdi для следующей функции

    push rdi ; дополнительно сохраняем rdi, чтобы не потерять после вызова
    call string_length ; длина ключа -> rax
    pop rdi

    inc rax ; добавляем 1, чтобы учесть нуль-терминатор
    add rdi, rax ; записываем в rdi ссылку на данные за ключом
    call print_string
    call print_newline
    call exit

    .long_key:
        add rsp, buffer_size
        mov rdi, long_key_msg
        call print_error
        call exit

    .empty_key:
        add rsp, buffer_size
        mov rdi, empty_key_msg
        call print_error
        call exit

    .no_match:
        add rsp, buffer_size
        mov rdi, no_match_msg
        call print_error
        call exit

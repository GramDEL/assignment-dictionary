global find_word
extern string_equals

section .text
; Два аргумента:
; в rdi - указатель на нуль-терминированную строку;
; в rsi - указатель на начало словаря.
; Возвращает в rax адрес начала вхождения в словарь или 0.
find_word:
    test rsi, rsi
    je .exit
    push rsi ; convention
    add rsi, 8 ; смещаемся на кол-во байтов, равное размеру dq, чтобы получить адрес ключа
    push rdi ; convention
    call string_equals
    pop rdi
    pop rsi
    test rax, rax
    jne .match
    mov rsi, [rsi] ; помещаем в rsi ссылку на следующий элемент
    jmp find_word
    .match:
        mov rax, rsi
    .exit:
        mov rax, 0
        ret
